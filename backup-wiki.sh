#!/bin/bash 
#Author: Domenico Caravaggio
# automatically backup
#
########################
#   CONFIG FILE
BASE_DIR=/wiki
DIR_LOG=/logs/wiki
LOG_PREFIX=backup-wiki-file
FILE_LOG=${DIR_LOG}/${LOG_PREFIX}.log
DIR_BACKUP=/backup
FILE_PREFIX=wiki-backup
FILE_BACKUP=${DIR_BACKUP}/${FILE_PREFIX}-$( date +'%Y-%m-%d-%s' )
FILE_BACKUP_GZ=${FILE_BACKUP}.tar.gz
DIR_WIKI=/wiki/www
################file backup

BACKUP_LIST[1]=${DIR_WIKI}/images
BACKUP_LIST[2]=${DIR_WIKI}/LocalSettings.php
BACKUP_LIST[3]=${DIR_WIKI}/ldap.json

#######################
function loging {
        DATE=$( date )
        TEXT="${@}"
        echo "${DATE} - ${TEXT}" 1>&2
        echo "${DATE} - ${TEXT}" >> ${FILE_LOG}

}
loging " ------- INIT SCRIPT -------"
loging " ------- config script -------"
loging " FILE_BACKUP  ${FILE_BACKUP}"
loging " FILE_LOG ${FILE_LOG}"
loging " DIR_WIKI ${DIR_WIKI}"
loging " -----------------------------"
loging " ----------- CREATE DIR ------------"
mkdir -p ${FILE_BACKUP}
loging " ----------- RSYINC FILE ------------"
INDEX=1
NUM_LIST=${#BACKUP_LIST[*]}
while [ ${INDEX} -le ${NUM_LIST} ]
do
loging " ${BACKUP_LIST[$INDEX]} to ${FILE_BACKUP}/ "
rsync -a -r ${BACKUP_LIST[$INDEX]} ${FILE_BACKUP}/
INDEX=$(( ${INDEX} + 1 ))
done
loging " ----------- TAR GZ FILE  ------------"
if [ ! -f "$FILE_BACKUP_GZ" ]; then
    loging " INFO: EXEC tar -zcvpf ${FILE_BACKUP_GZ} ${FILE_BACKUP}"
    tar -zcvpf ${FILE_BACKUP_GZ} ${FILE_BACKUP}
    loging " INFO: EXEC  rm -r -f ${FILE_BACKUP}"
    rm -r -f ${FILE_BACKUP}
else
    loging "ERROR exists ${FILE_BACKUP_GZ}"
    loging " INFO: EXEC  rm -r -f ${FILE_BACKUP}"
    rm -r -f ${FILE_BACKUP}
    exit 1
fi
loging " ------ delete old BACKUP FILE ------"
loging " find ${DIR_BACKUP} -type f -name "${FILE_PREFIX}" -mtime +7 -exec rm -f {} \; "
find ${DIR_BACKUP} -type f -name "${FILE_PREFIX}*.gz" -mtime +7 -exec rm -f {} \;
loging " ------ EXIT SCRIPT ------"
exit 0
