#!/bin/bash 
#Author: Domenico Caravaggio
# automatically dump
#
########################
#   CONFIG FILE
BASE_DIR=
DIR_LOG=
LOG_PREFIX=dump-wiki-db
FILE_LOG=${DIR_LOG}/${LOG_PREFIX}.log
DIR_DUMP=/backup
FILE_DUMP_PREFIX=wiki-dump
FILE_DUMP=${DIR_DUMP}/${FILE_DUMP_PREFIX}-$( date +'%Y-%m-%d-%s' )
########################
########################
# DB USER AND DB NAME
USER_DB=''
PASS_DB=''
NAME_DB=''
#######################
function loging {
        DATE=$( date )
        TEXT="${@}"
        echo "${DATE} - ${TEXT}" 1>&2
        echo "${DATE} - ${TEXT}" >> ${FILE_LOG}

}
loging " ------- INIT SCRIPT -------"
loging " ------- config script -------"
loging " FILE_DUMP  ${FILE_DUMP}"
loging " FILE_LOG ${FILE_LOG}"
loging " USER_DB ${USER_DB}"
loging " NAME_DB ${NAME_DB}"
loging " -----------------------------"

loging " ------- exec dump data -------"
mysqldump --user=${USER_DB} --password=${PASS_DB} ${NAME_DB} --xml >  ${FILE_DUMP}.xml
mysqldump --user=${USER_DB} --password=${PASS_DB} ${NAME_DB} >  ${FILE_DUMP}.sql
loging " -----------------------------"
loging " ------ delete old dump ------"
find ${DIR_DUMP} -type f -name "${FILE_DUMP_PREFIX}*.xml" -mtime +7 -exec rm -f {} \;
find ${DIR_DUMP} -type f -name "${FILE_DUMP_PREFIX}*.sql" -mtime +7 -exec rm -f {} \;
loging " ------ EXIT SCRIPT ------"
exit 0
